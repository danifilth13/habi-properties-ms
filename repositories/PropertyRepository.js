const PropertyRepository = module.exports;
const db = require('../utils/DB');

const {
  PROPERTY,
  STATUS,
  STATUS_HISTORY,
} = require('./TableNames');

const defaultColumns = [
  `${PROPERTY}.address`,
  `${PROPERTY}.city`,
  `${PROPERTY}.price`,
  `${PROPERTY}.description`,
];

PropertyRepository.getProperties = (
  limit,
  page,
  filters = {
    buildYear: '',
    city: '',
    state: '',
  },
) => {
  const query = db(`${STATUS_HISTORY} as sh`)
    .select(defaultColumns)
    .joinRaw(`JOIN (SELECT property_id, status_id,  max(update_date) as MaxDate 
from status_history group by property_id) jsh
on sh.property_id = jsh.property_id and sh.update_date = jsh.MaxDate`)
    .innerJoin(
      PROPERTY,
      'sh.property_id',
      '=',
      `${PROPERTY}.id`,
    )
    .innerJoin(
      STATUS,
      'sh.status_id',
      '=',
      `${STATUS}.id`,
    )
    .where((qb) => {
      if (filters.buildYear) {
        qb.where(db.raw(`${PROPERTY}.year`), `${filters.buildYear}`);
      }
      if (filters.city) {
        qb.where(db.raw(`${PROPERTY}.city`), '=', `${filters.city.toLowerCase()}`);
      }
      if (filters.state) {
        qb.where('sh.status_id', Number(filters.state));
      }
    })
    .limit(limit)
    .offset(page);

  return query;
};
