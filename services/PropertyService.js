const PropertyService = module.exports;

const createHttpError = require('http-errors');
const PropertyRepository = require('../repositories/PropertyRepository');

const logName = 'PropertiesService';

PropertyService.getProperties = async (
  limit,
  page,
  filters = {
    buildYear: '',
    city: '',
    state: '',
  },
  options = {},
) => {
  const { logger = console.log } = options;
  logger(`${logName}.getProperties with filter: ${JSON.stringify(filters)}`);
  const limitNumber = Number(limit);
  const offsetNumber = Number(page);
  logger(
    logName,
    `..executing: getProperties with limit: ${limitNumber} and offset: ${offsetNumber}`,
  );

  if (Number.isNaN(-limitNumber) || Number.isNaN(-offsetNumber)) {
    throw createHttpError(400, 'Invalid limit/offset query params');
  }

  if (offsetNumber < 0 || limitNumber < 1 || limitNumber > 50) {
    throw createHttpError(400, 'Limit must be between (1 - 50) and Offset must be at least 0');
  }

  const response = await PropertyRepository.getProperties(
    limit,
    page,
    filters,
  );

  return response;
};
