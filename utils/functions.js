const COMPRANDO = 1;
const COMPRADO = 2;
const PRE_VENTA = 3;
const EN_VENTA = 4;
const VENDIDO = 5;

const availableStatus = [
  PRE_VENTA,
  EN_VENTA,
  VENDIDO,
];

function validatePropertyStatus(status) {
  if (!availableStatus.includes(status)) {
    return '';
  }
  return status;
}

const isObject = (a) => (!!a) && (a.constructor === Object);

module.exports = {
  COMPRANDO,
  COMPRADO,
  PRE_VENTA,
  EN_VENTA,
  VENDIDO,
  validatePropertyStatus,
  isObject,
};
