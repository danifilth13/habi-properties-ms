const knex = require('knex');
const { databaseConfig } = require('../config/db');
// dotenv.config({ path: './config/config.env' });

const DB = knex(databaseConfig);

module.exports = DB;
