const getErrorMessage = (error) => {
  if (error.message) {
    return error.message;
  }

  if (error.stack) {
    return error.stack;
  }

  if (typeof error.toString === 'function') {
    return error.toString();
  }

  return '';
};

const logErrorMessage = (error) => {
  console.error(error);
};

const isErrorStatusCode = (statusCode) => statusCode >= 400 && statusCode < 600;

const getHttpStatusCode = (error, response) => {
  const statusCodeFromError = error.status || error.statusCode;
  if (isErrorStatusCode(statusCodeFromError)) {
    return statusCodeFromError;
  }

  const statusCodeFromResponse = response.statusCode;
  if (isErrorStatusCode(statusCodeFromResponse)) {
    return statusCodeFromResponse;
  }

  return 500;
};

module.exports = {
  getErrorMessage,
  logErrorMessage,
  isErrorStatusCode,
  getHttpStatusCode,
};
