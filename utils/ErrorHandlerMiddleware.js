const ErrorHandlerMiddleware = module.exports;
const { getErrorMessage, logErrorMessage, getHttpStatusCode } = require('./ErrorManager');

const NODE_ENVIRONMENT = process.env.NODE_ENV || 'development';

ErrorHandlerMiddleware.errorHandlerMiddleware = (err, req, res, next) => {
  const errorMessage = getErrorMessage(err);

  logErrorMessage(errorMessage);

  if (res.headersSent) {
    return next(err);
  }

  const errorResponse = {
    statusCode: getHttpStatusCode(err, res),
    body: undefined,
  };

  if (NODE_ENVIRONMENT !== 'production') {
    errorResponse.body = errorMessage;
  }

  res.status(errorResponse.statusCode);

  res.format({
    'application/json': () => {
      res.json({ message: errorResponse.body });
    },
    default: () => {
      res.type('text/plain').send(errorResponse.body);
    },
  });

  return next();
};
