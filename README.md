# README #

Descripción:
* Microservicio basado en consultar propiedades de HABI.

Config file:
Crear archivo config.env dentro de la carpeta config con el siguiente contenido de ENV_VARS:

NODE_ENV= development
APP_ROOT_PATH = /api/habi/
PORT = 3000
COUNTRY = CO
DB_CONNECTION = mysql://3.130.126.210:3309:admin:HANrhz5u7e3jKqVQ:@mysql/habi_db
DB_HOST=3.130.126.210
DB_PORT=3309
DB_USER=admin
DB_PSW=HANrhz5u7e3jKqVQ
DB_NAME=habi_db

Recursos:
* Implementa un archivo .json (resources/filters.json) proporcionando los datos a filtrar el inmueble por el usuario.
* Cambiar los parametros deseados según el archivo:
	"city": "MEDELLIN", -> ciudad
	"buildYear": 2011, -> año de construcción
	"status": 4, -> estado
	"page": 0, -> pagina
	"limit": 10 -> limite de pagina


Tecnología implementada:
* NodeJs.

Justificación:
* A pesar de que la base de datos está implementada bajo el build migrations de django, se realiza la solución en NodeJs ya que es parte del stack que requiere HABI y quise implementar el esquema de API que provee Express.js ya que es bastante sencillo, robusto y fluido al momento de implementar async/ await procesamientos a buena velocidad de respuesta.

Correr Proyecto:
* npm i (instalar dependencias)
* npm run dev
* localhost:3000/api/habi/properties/availability

Correr Tests:
* npm run test

Diagrama Likes - Properties:

* https://lucid.app/lucidchart/f0f935ba-f9fd-4386-a86d-2af14362169b/edit?beaconFlowId=97CDE5127921984D&page=0_0#