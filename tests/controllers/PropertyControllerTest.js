const assert = require('assert');
const chai = require('chai');
const sinon = require('sinon');
const chaiHttp = require('chai-http');
const createHttpError = require('http-errors');

const {
  describe,
  it,
  before,
  afterEach,
  after,
} = require('mocha');
const PropertyRepository = require('../../repositories/PropertyRepository');
const app = require('../../server');

const {
  APP_ROOT_PATH,
} = process.env;

describe('StoresController', () => {
  chai.use(chaiHttp);
  const sandbox = sinon.createSandbox();

  let serviceFail = false;
  const propertyeEmpty = false;
  const property = [{
    address: 'carrera 100 #15-90',
    city: 'medellin',
    price: 325000000,
    description: 'Amplio apartamento en conjunto cerrado',
  }];

  before(() => {
    sandbox.stub(PropertyRepository, 'getProperties').callsFake(() => new Promise(
      (resolve, reject) => {
        if (serviceFail) {
          reject(createHttpError(500, 'Internal Error'));
        }
        if (propertyeEmpty) {
          resolve([]);
        } else if (!property) {
          resolve([]);
        } else {
          resolve([property]);
        }
      },
    ));
  });

  afterEach(() => {
    serviceFail = false;
  });

  after(() => {
    sandbox.restore();
  });

  describe('Get properties', () => {
    it('Should get a 200 OK', async () => {
      const response = await chai.request(app)
        .get(`${APP_ROOT_PATH}/properties/availability`)
        .send();

      assert.strictEqual(response.status, 200);
      assert.deepStrictEqual(response.body, [property]);
    });

    it('Should get a 500 error if service fail', async () => {
      serviceFail = true;
      const response = await chai.request(app)
        .get(`${APP_ROOT_PATH}/properties/availability`)
        .send();

      assert.strictEqual(response.status, 500);
    });
  });
});
