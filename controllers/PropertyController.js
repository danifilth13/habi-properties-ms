const PropertyController = module.exports;
const createHttpError = require('http-errors');
const PropertyService = require('../services/PropertyService');
const {
  validatePropertyStatus,
} = require('../utils/functions');
const filterData = require('../resources/filters.json');

const logName = 'PropertyController: ';

PropertyController.getProperties = async (req, res, next) => {
  const logger = req.log || console.log;
  const options = { logger: logger.bind(logger, logName) };

  try {
    const {
      limit = '',
      page = '',
      buildYear = '',
      city = '',
      status = '',
    } = filterData;

    const state = validatePropertyStatus(status);

    const response = await PropertyService.getProperties(
      limit,
      page, {
        buildYear,
        city,
        state,
      },
      options,
    );

    return res.send(response);
  } catch (e) {
    const httpError = createHttpError(500, e);
    return next(httpError);
  }
};
