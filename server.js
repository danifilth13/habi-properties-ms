const express = require('express');
const dotenv = require('dotenv');
const log4js = require('log4js');
const path = require('path');
const { errorHandlerMiddleware } = require('./utils/ErrorHandlerMiddleware');

dotenv.config({ path: './config/config.env' });

const app = express();
const PORT = process.env.PORT || 3000;

const APP_NAME = 'habi-api';
const logger = log4js.getLogger(APP_NAME);

const property = require('./routes/property');

log4js.configure({
  appenders: {
    console: { type: 'console' },
    file: { type: 'file', filename: path.join(__dirname, '../../logs/server.log') },
  },
  categories: {
    default: { appenders: ['file', 'console'], level: 'debug' },
  },
});

app.use(log4js.connectLogger(logger, { level: 'auto' }));
app.use('/api/habi', property);
app.use(errorHandlerMiddleware);
app.listen(
  PORT,
  logger.info(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`),
);

module.exports = app;
