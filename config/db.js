const { env } = process;
const {
  TIMEZONE = 'America/Bogota',
  COUNTRY,
  DB_CONNECTION,
  DB_HOST,
  DB_PORT,
  DB_USER,
  DB_PSW,
  DB_NAME,
  DB_CONNECTION_MAX_POOLSIZE = 5,
  LANGUAGE = 'es',
} = env;

module.exports = {
  TIMEZONE,
  COUNTRY,
  DB_CONNECTION,
  DB_CONNECTION_MAX_POOLSIZE,
  databaseConfig: {
    client: 'mysql',
    connection: {
      host: DB_HOST,
      user: DB_USER,
      port: DB_PORT,
      password: DB_PSW,
      database: DB_NAME,
      // DB_CONNECTION,
    },
    pool: {
      min: 1,
      max: 10,
    },
    acquireConnectionTimeout: 5000,
  },
  LANGUAGE,
};
